#include "common.h"
#include "service.h"

#include <string.h>

int main(){
  const char* log = getenv("_SYSLOG");
  
  if(!log || strcmp(log, "only") != 0){
    openlog(PROJECT_STRING, LOG_CONS | LOG_NDELAY | LOG_PERROR, LOG_DAEMON);
  }
  
  service_t base;
  
  service_init(&base);
  service_loop(&base);
  service_done(&base);
  
  return 0;
}
