#ifdef ALSA_SUPPORT

#include "common.h"
#include "audio.h"
#include "alsap.h"

#include <asoundlib.h>

typedef struct alsap_stm_ alsap_stm_t;

struct alsap_stm_ {
  audio_ctx_t *ctx;
  
  char* dev;
  snd_pcm_t* pcm;
};

void alsap_stm_done(alsap_stm_t* stm){
  if(stm->pcm){
    syslog(LOG_INFO, "Close alsa device (%s)", stm->dev);
    
    snd_pcm_close(stm->pcm);
    stm->pcm = NULL;
  }
  
  if(stm->dev){
    free(stm->dev);
  }
  
  free(stm);
}

alsap_stm_t* alsap_stm_init(audio_ctx_t* ctx, unsigned int rate, const char* device, const char* stream){
  alsap_stm_t* stm = malloc(sizeof(alsap_stm_t));
  
  stm->ctx = ctx;
  stm->dev = device ? strdup(device) : NULL;
  
  syslog(LOG_INFO, "Opening alsa device (%s) with latency (%d) and rate (%d)", stm->dev, ALSA_LATENCY, rate);
  
  int err;
  
  if((err = snd_pcm_open(&stm->pcm, stm->dev, SND_PCM_STREAM_PLAYBACK, SND_PCM_NONBLOCK)) < 0){
    syslog(LOG_ERR, "Cannot open audio device (%s)", snd_strerror(err));
    goto done;
  }
  
  if((err = snd_pcm_set_params(stm->pcm,
                               SND_PCM_FORMAT_S16_LE,
                               SND_PCM_ACCESS_RW_INTERLEAVED,
                               1, rate, 1, ALSA_LATENCY)) < 0){
    syslog(LOG_ERR, "Cannot set parameters (%s)", snd_strerror(err));
    goto done;
  }
  
  syslog(LOG_INFO, "Alsa device (%s) opened", stm->dev);
  
 done:
  //snd_pcm_hw_params_free(hw_params);
  
  if(err < 0){
    alsap_stm_done(stm);
    return NULL;
  }
  
  return stm;
}

static const char* alsap_state(snd_pcm_state_t state){
  switch(state){
  case SND_PCM_STATE_OPEN: return "open";
  case SND_PCM_STATE_SETUP: return "setup";
  case SND_PCM_STATE_PREPARED: return "prepared";
  case SND_PCM_STATE_RUNNING: return "running";
  case SND_PCM_STATE_XRUN: return "xrun";
  case SND_PCM_STATE_DRAINING: return "draining";
  case SND_PCM_STATE_PAUSED: return "paused";
  case SND_PCM_STATE_SUSPENDED: return "suspended";
  case SND_PCM_STATE_DISCONNECTED: return "disconnected";
  }
  return "undefined";
}

size_t alsap_stm_req(alsap_stm_t* stm, audio_req_t req){
  if(!stm->pcm){
    /* emulate skip */
    return 0;
  }

  int err = 0;
  snd_pcm_state_t state = snd_pcm_state(stm->pcm);
  
  LOG("Alsa state: %s", alsap_state(state));
  
  if(req == audio_stop){
#if 0
    /* we need to preserve all unplayed data */
    if(state == SND_PCM_STATE_RUNNING){
      LOG("Soft stop audio interface (%s)", stm->dev);
      if((err = snd_pcm_drain(stm->pcm)) < 0){
        if(err != -ESTRPIPE){
          syslog(LOG_ERR, "Cannot drain audio interface (%s)", snd_strerror(err));
        }
      }
    }
#endif
    return 0;
  }

  if(req == audio_drop){
    /* we need to drop all unplayed data */
    if(state == SND_PCM_STATE_RUNNING){
      LOG("Hard stop audio interface (%s)", stm->dev);
      if((err = snd_pcm_drop(stm->pcm)) < 0){
        syslog(LOG_ERR, "Cannot drop audio interface (%s)", snd_strerror(err));
      }
    }
    return 0;
  }
  
  /* if(req == audio_drop) */
  
  if(state == SND_PCM_STATE_SETUP || state == SND_PCM_STATE_XRUN){
    /* we need to prepare playing */
    LOG("Prepare audio interface (%s)", stm->dev);
    if((err = snd_pcm_prepare(stm->pcm)) < 0){
      syslog(LOG_ERR, "Cannot prepare audio interface for use (%s)", snd_strerror(err));
      return 0;
    }
  }
  
  state = snd_pcm_state(stm->pcm);
  LOG("Alsa state: %s", alsap_state(state));
  
  /*
  if(state == SND_PCM_STATE_DRAINING){
    LOG("Restart audio interface (%s)", stm->dev);
    if((err = snd_pcm_start(stm->pcm)) < 0){
      syslog(LOG_ERR, "Cannot restart audio interface (%s)", snd_strerror(err));
      return 0;
    }
  }
  
  state = snd_pcm_state(stm->pcm);
  LOG("Alsa state: %s", alsap_state(state));
  */
  
  if(state == SND_PCM_STATE_PREPARED || state == SND_PCM_STATE_RUNNING || state == SND_PCM_STATE_DRAINING){
    LOG("Request audio interface (%s)", stm->dev);
    if((err = snd_pcm_avail(stm->pcm)) < 0){
      if(err == -EPIPE){
        syslog(LOG_WARNING, "An underrun occured on request");
      }else{
        syslog(LOG_ERR, "Request failed (%s) State (%d)", snd_strerror(err), snd_pcm_state(stm->pcm));
      }
      return 0;
    }
  }
  
  return err << 1;
}

void alsap_stm_out(alsap_stm_t* stm, void* data, size_t size){
  size >>= 1;
  
  int err;
  snd_pcm_state_t state = snd_pcm_state(stm->pcm);
  
  LOG("Alsa state: %s", alsap_state(state));
  
  if(state == SND_PCM_STATE_PREPARED || state == SND_PCM_STATE_RUNNING){
    LOG("Output frames (%d) to audio interface (%s)", size, stm->dev);
    if((err = snd_pcm_writei(stm->pcm, data, size)) < 0){
      if(err == -EPIPE){
        syslog(LOG_WARNING, "An underrun occured on write");
      }else{
        syslog(LOG_ERR, "Write failed (%s) State (%d)", snd_strerror(err), snd_pcm_state(stm->pcm));
      }
    }
  }
  
  state = snd_pcm_state(stm->pcm);
  LOG("Alsa state: %s", alsap_state(state));
}

audio_io(alsap);

audio_ctx_t alsap_ctx = {
  /*.io = */&alsap_io
};

#endif//ALSA_SUPPORT
