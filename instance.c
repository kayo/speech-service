#include "common.h"
#include "instance.h"

#include <string.h>

struct phrase_ {
  chain_cookie;
  
  char* text; /* utf8 text */
};

static phrase_t* phrase_init(const char* text){
  phrase_t* phrase = malloc(sizeof(phrase_t));
  
  phrase->text = strdup(text);
  
  return phrase;
}

static void phrase_done(phrase_t* phrase){
  free(phrase->text);
  free(phrase);
}

static void _instance_play(instance_t* instance){
  if(event_pending(&instance->ev_play, EV_TIMEOUT, NULL)){
    return;
  }
  LOG("Instance (%s) play begin", table_key(instance));
  event_add(&instance->ev_play, &instance->play_delay);
}

static void _instance_stop(instance_t* instance){
  if(!event_pending(&instance->ev_play, EV_TIMEOUT, NULL)){
    return;
  }
  LOG("Instance (%s) play end", table_key(instance));
  event_del(&instance->ev_play);
  audio_stm_req(instance->audio, audio_stop);
}

static void _instance_dbus_deque(instance_t *instance){
  if(!instance->dbus_conn){
    return;
  }
  
  DBusMessage *event = message_helper_create(&instance->deque, NULL);
  
  int active = !!instance->operation;
  uint32_t count = chain_len(instance->phrase);
  
  dbus_message_append_args(event, DBUS_TYPE_BOOLEAN, &active,
                           DBUS_TYPE_UINT32, &count, DBUS_TYPE_INVALID);
  
  message_helper_output(instance->dbus_conn, event);
}

static void _instance_process(instance_t* instance){
  if(instance->operation || !instance->phrase){
    return;
  }
  
  instance->operation = synth_render(instance->synth, instance->phrase->text, instance->play_chunk * 2);
  chain_deq(instance->phrase, phrase);
  _instance_dbus_deque(instance);
  
  _instance_play(instance);
}

static void _instance_complete(instance_t* instance){
  if(!instance->operation){
    return;
  }
  
  synth_delete(instance->operation);
  instance->operation = NULL;
  
  if(instance->phrase){
    _instance_process(instance);
  }else{
    _instance_stop(instance);
  }
}

static void _instance_play_cb(evutil_socket_t fd, short ev, void *ud){
  instance_t *instance = ud;
  
  if(!instance->operation){
    return;
  }

  size_t free = audio_stm_req(instance->audio, audio_play);

  if(free == 0){
    /* overrun */
    return;
  }
  
  size_t size;
  char data[instance->play_chunk];
  
  for(; ; ){
    size = instance->play_chunk;
    if(size > free){
      size = free;
    }
    if(size == 0){
      break;
    }
    size = synth_op_get(instance->operation, data, size);
    if(size == 0){
      break;
    }
    LOG("Play fragment (%s) chunk (%d)", table_key(instance), (int)size);
    audio_stm_out(instance->audio, data, size);
    free -= size;
  }
  
  LOG("state: %s, len: %d", instance->operation->state == SYNTH_OP_PROC ? "proc" :
      instance->operation->state == SYNTH_OP_STOP ? "stop" :
      instance->operation->state == SYNTH_OP_DONE ? "done" : "init", synth_op_len(instance->operation));
  if(instance->operation->state != SYNTH_OP_PROC &&
     synth_op_len(instance->operation) == 0){
    /* fully rendered and completely played */
    _instance_complete(instance);
  }
}

static void _instance_shift(instance_t* instance, int reset, size_t count){
  if(reset && instance->operation){
    synth_op_cancel(instance->operation);
    
#if 0
    size_t size;
    char data[instance->play_chunk];
    for(; (size = synth_op_get(instance->operation, data, sizeof(data))); );
    
    audio_stm_req(instance->audio, audio_stop);
    //_instance_complete(instance);
#endif
  }
  
  unsigned dequed = 0;
  
  if(!count){
    count = chain_len(instance->phrase);
  }
  
  for(; instance->phrase && count; count--, dequed++){
    chain_deq(instance->phrase, phrase);
  }
  
  if(reset || dequed){
    _instance_dbus_deque(instance);
  }
}

static int _instance_dbus_introspect(DBusConnection *connection, DBusMessage *message, void *ud){
  instance_t *instance = ud;
  
  DBusMessage *reply = message_helper_return(&instance->introspect, message);
  
  message_helper_i11n_iface(DBUS_INTERFACE_INTROSPECTABLE);
  
  message_helper_i11n_method(instance->introspect.name);
  message_helper_i11n_method_arg("xml", "s", MESSAGE_OUTPUT);
  message_helper_i11n_method(NULL);
  
  message_helper_i11n_iface(NULL);
  
  message_helper_i11n_iface(DBUS_INTERFACE_SPEAKER);
  
  message_helper_i11n_method(instance->state.name);
  message_helper_i11n_method_arg("active", "as", MESSAGE_OUTPUT);
  message_helper_i11n_method_arg("queued", "as", MESSAGE_OUTPUT);
  message_helper_i11n_method(NULL);
  
  message_helper_i11n_method(instance->queue.name);
  message_helper_i11n_method_arg("phrase", "s", MESSAGE_INPUT);
  message_helper_i11n_method(NULL);
  
  message_helper_i11n_method(instance->shift.name);
  message_helper_i11n_method_arg("reset", "b", MESSAGE_INPUT);
  message_helper_i11n_method_arg("count", "u", MESSAGE_INPUT);
  message_helper_i11n_method(NULL);
  
  message_helper_i11n_signal(instance->deque.name);
  message_helper_i11n_signal_arg("active", "b");
  message_helper_i11n_signal_arg("queued", "u");
  message_helper_i11n_signal(NULL);
  
  message_helper_i11n_iface(NULL);
  
  const char *i11n = message_helper_i11n();
  
  dbus_message_append_args(reply, DBUS_TYPE_STRING, &i11n, DBUS_TYPE_INVALID);
  message_helper_output(connection, reply);
  
  return 1;
}

static int _instance_dbus_state(DBusConnection *connection, DBusMessage *message, void *ud){
  instance_t *instance = ud;
  
  DBusMessage *reply = message_helper_return(&instance->state, message);
  DBusMessageIter reply_iter, phrase_iter;

  dbus_message_iter_init_append(reply, &reply_iter);
  dbus_message_iter_open_container(&reply_iter, DBUS_TYPE_ARRAY, "s", &phrase_iter);
  if(instance->operation){
    dbus_message_iter_append_basic(&phrase_iter, DBUS_TYPE_STRING, &instance->operation->text);
  }
  dbus_message_iter_close_container(&reply_iter, &phrase_iter);
  
  dbus_message_iter_init_append(reply, &reply_iter);
  dbus_message_iter_open_container(&reply_iter, DBUS_TYPE_ARRAY, "s", &phrase_iter);
  table_each(instance->phrase, phrase){
    dbus_message_iter_append_basic(&phrase_iter, DBUS_TYPE_STRING, &phrase->text);
  }
  dbus_message_iter_close_container(&reply_iter, &phrase_iter);
  
  message_helper_output(connection, reply);
  
  return 1;
}

static int _instance_dbus_queue(DBusConnection *connection, DBusMessage *message, void *ud){
  instance_t *instance = ud;
  
  const char* text = NULL;
  dbus_message_get_args(message, NULL, DBUS_TYPE_STRING, &text, DBUS_TYPE_INVALID);
  
  if(text){
    chain_add(instance->phrase, phrase, text);
    _instance_process(instance);
  }
  
  DBusMessage *reply = message_helper_return(&instance->queue, message);
  message_helper_output(connection, reply);
  
  return 1;
}

static int _instance_dbus_shift(DBusConnection *connection, DBusMessage *message, void *ud){
  instance_t *instance = ud;
  
  int reset = 0;
  uint32_t count = 0;
  dbus_message_get_args(message, NULL, DBUS_TYPE_BOOLEAN, &reset,
                        DBUS_TYPE_UINT32, &count, DBUS_TYPE_INVALID);
  
  _instance_shift(instance, reset, count);
  
  DBusMessage *reply = message_helper_return(&instance->shift, message);
  message_helper_output(connection, reply);
  
  return 1;
}

void instance_dbus_attach(instance_t* instance, DBusConnection* connection){
  instance->dbus_conn = connection;
  
  syslog(LOG_INFO, "Attaching Instance (%s)", instance->path);
  
  message_helper_attach(&instance->introspect, instance->dbus_conn);
  message_helper_attach(&instance->state, instance->dbus_conn);
  message_helper_attach(&instance->queue, instance->dbus_conn);
  message_helper_attach(&instance->shift, instance->dbus_conn);
}

void instance_dbus_detach(instance_t* instance, DBusConnection* connection){
  syslog(LOG_INFO, "Detaching Instance (%s)", instance->path);
  
  message_helper_detach(&instance->introspect, instance->dbus_conn);
  message_helper_detach(&instance->state, instance->dbus_conn);
  message_helper_detach(&instance->queue, instance->dbus_conn);
  message_helper_detach(&instance->shift, instance->dbus_conn);
  
  instance->dbus_conn = NULL;
}

instance_t* instance_init(char* name, struct event_base* ev_base, synth_t* synth, audio_stm_t* audio, cache_t* cache){
  instance_t* instance = malloc(sizeof(instance_t));
  
  table_key(instance) = name;
  
  instance->path = message_helper_mkpath(NULL, table_key(instance));
  
  syslog(LOG_INFO, "Creating instance (%s)", table_key(instance));
  
  instance->synth = synth;
  instance->audio = audio;
  
  instance->operation = NULL;
  
  chain_init(instance->phrase);
  
  instance->play_delay.tv_sec = SAMPLE_DELAY / 1000000;
  instance->play_delay.tv_usec = SAMPLE_DELAY % 1000000;
  
  instance->play_chunk = synth->rate / (1000000 / 2 / SAMPLE_DELAY);
  LOG("Play delay (%d us) chunk (%d bytes)", (int)instance->play_delay.tv_usec, (int)instance->play_chunk);
  
  event_assign(&instance->ev_play, ev_base, -1, EV_TIMEOUT | EV_PERSIST, _instance_play_cb, instance);
  event_priority_set(&instance->ev_play, 2);
  
  /* Init DBus */
  message_helper_init(&instance->introspect,
                      DBUS_SERVICE_SPEECH,
                      MESSAGE_METHOD,
                      instance->path,
                      DBUS_INTERFACE_INTROSPECTABLE,
                      "Introspect",
                      _instance_dbus_introspect,
                      instance);
  
  message_helper_init(&instance->state,
                      DBUS_SERVICE_SPEECH,
                      MESSAGE_METHOD,
                      instance->path,
                      DBUS_INTERFACE_SPEAKER,
                      "State",
                      _instance_dbus_state,
                      instance);
  
  message_helper_init(&instance->queue,
                      DBUS_SERVICE_SPEECH,
                      MESSAGE_METHOD,
                      instance->path,
                      DBUS_INTERFACE_SPEAKER,
                      "Queue",
                      _instance_dbus_queue,
                      instance);
  
  message_helper_init(&instance->shift,
                      DBUS_SERVICE_SPEECH,
                      MESSAGE_METHOD,
                      instance->path,
                      DBUS_INTERFACE_SPEAKER,
                      "Shift",
                      _instance_dbus_shift,
                      instance);
  
  message_helper_init(&instance->deque,
                      DBUS_SERVICE_SPEECH,
                      MESSAGE_SIGNAL,
                      instance->path,
                      DBUS_INTERFACE_SPEAKER,
                      "Deque",
                      NULL,
                      instance);
  
  return instance;
}

void instance_done(instance_t* instance){
  table_done(instance->phrase);
  
  syslog(LOG_INFO, "Deleting instance (%s)", table_key(instance));
  
  table_key_done(instance);
  
  audio_stm_done(instance->audio);
  
  if(instance->path){
    free(instance->path);
    instance->path = NULL;
  }
}
