ifeq ($(audio.backend),pulse)
  def+=PULSE_SUPPORT=1
  def+=PULSE_LATENCY=$(audio.latency)
  pkg.deps+=libpulse
endif
