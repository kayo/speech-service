#ifndef __synth_h__
#define __synth_h__ "synth.h"

#include "table.h"
#include "cache.h"

#include <pthread.h>
#include <semaphore.h>
#include "ringbuf.h"

/* synth object */
typedef struct synth_ synth_t;

/* operation data callback data = NULL, size = 0 means end of operation */
typedef void(*synth_cb_f)(void* data, size_t size, void* ud);
/* operation object */
typedef enum synth_op_state_ synth_op_state_t;
typedef struct synth_op_ synth_op_t;
typedef struct synth_api_impl_ synth_api_impl_t;
typedef struct synth_api_ synth_api_t;

typedef synth_t* synth_api_init_f(struct event_base* ev_base, json_object* config);
typedef void synth_api_done_f(synth_t* self);
typedef synth_op_t* synth_api_op_init_f(synth_t* self, const char* text, size_t size);
typedef void synth_api_op_done_f(synth_op_t* op);

struct synth_api_impl_ {
  synth_api_init_f* init;
  synth_api_done_f* done;
  
  synth_api_op_init_f* op_init;
  synth_api_op_done_f* op_done;
};

struct synth_api_ {
  table_cookie;
  
  void* handle;
  unsigned int usage;
  
  synth_api_impl_t* impl;
};

#define synth_api_prefix __synth_
#define synth_api_suffix _api__

#define synth_api_cat3_(a, b, c) a ## b ## c
#define synth_api_cat3(a, b, c) synth_api_cat3_(a, b, c)
#define synth_api_name(name) synth_api_cat3(synth_api_prefix, name, synth_api_suffix)
#define synth_api_impl(name, init, done, op_init, op_done)        \
  synth_api_impl_t synth_api_name(name) = {                       \
    (synth_api_init_f*)init, (synth_api_done_f*)done,             \
    (synth_api_op_init_f*)op_init, (synth_api_op_done_f*)op_done  \
  }
#define synth_api_call(api, func, args...) (api)->impl->func(args)

synth_t* synth_init(const char* name, struct event_base* ev_base, json_object* config);
void synth_done(synth_t* self);

synth_op_t* synth_render(synth_t* self, const char* text, size_t size);
void synth_delete(synth_op_t* op);

size_t synth_op_len(synth_op_t* op);
size_t synth_op_get(synth_op_t* op, void* data, size_t size); /**< non-blocking data get */
void synth_op_cancel(synth_op_t* op);

enum synth_op_state_ {
  SYNTH_OP_INIT,
  SYNTH_OP_PROC,
  SYNTH_OP_STOP,
  SYNTH_OP_DONE
};

struct synth_op_ {
  char* text;
  
  synth_t* synth;
  
  sem_t buf_sem; /**< samples access semaphore */
  sem_t owf_sem; /**< buffer overflow semaphore */
  rb_dyn_t buf;  /**< ring buffer of samples */
  
  synth_op_state_t state;
};

#define synth_op_(o, f) (((synth_op_t*)(o))->f)

#define synth_op_cookie struct synth_op_ __cookie__

void synth_op_init_(synth_op_t* op, synth_t* synth, const char* text, size_t size);
#define synth_op_init(op, synth, text, size) synth_op_init_((synth_op_t*)(op), (synth_t*)(synth), text, size)
void synth_op_done_(synth_op_t* op);
#define synth_op_done(op) synth_op_done_((synth_op_t*)(op))
size_t synth_op_put_block_(synth_op_t* op, const void* data, size_t size); /**< blocking data put */
#define synth_op_put_block(op, data, size) synth_op_put_block_((synth_op_t*)(op), data, size)
void synth_op_begin_(synth_op_t* op);
#define synth_op_begin(op) synth_op_begin_((synth_op_t*)(op))
void synth_op_end_(synth_op_t* op);
#define synth_op_end(op) synth_op_end_((synth_op_t*)(op))

struct synth_ {
  table_cookie;
  
  unsigned int rate;
  
  //synth_op_t* seq;
  synth_api_t* api;
  //cache_t cache;
  
  char died:1;
};

#define synth_(s, f) (((synth_t*)(s))->f)

#define synth_cookie struct synth_ __cookie__

#endif//__synth_h__
