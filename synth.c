#include "common.h"

#include "synth.h"

#include <string.h>
#include <dlfcn.h>

#define _synth_api_str_(def) #def
#define _synth_api_str(def) _synth_api_str_(def)
static const char _synth_api_prefix[] = _synth_api_str(synth_api_prefix);
static const char _synth_api_suffix[] = _synth_api_str(synth_api_suffix);

static synth_api_t* synth_api_init(const char* name){
  static char path[255];
  
  const char* root = getenv("_PLUGINS");
  if(!root){
    root = ".";
  }
  
  char* p = path;
  
  strcpy(p, root);
  p += strlen(root);
  *p++ = '/';
  
  strcpy(p, name);
  p += strlen(name);
  strcpy(p, ".so");
  
  syslog(LOG_INFO, "Loading synth plugin (%s) from (%s)", name, path);
  
  void* handle = dlopen(path, RTLD_NOW);
  
  if(!handle){
    syslog(LOG_ERR, "Unable to load (%s) synth plugin (%s)", name, dlerror());
    return NULL;
  }
  
  strcpy(p = path, _synth_api_prefix);
  p += sizeof(_synth_api_prefix) - 1;
  strcpy(p, name);
  p += strlen(name);
  strcpy(p, _synth_api_suffix);
  
  synth_api_impl_t* impl = dlsym(handle, path);
  
  if(!impl){
    syslog(LOG_ERR, "Synth API implementation not found in (%s) plugin (%s)", name, dlerror());
    dlclose(handle);
    return NULL;
  }

  if(!impl->init){
    syslog(LOG_ERR, "Synth API implementation of init() in (%s) not found", name);
    dlclose(handle);
    return NULL;
  }
  
  synth_api_t* api = malloc(sizeof(synth_api_t));
  
  api->impl = impl;
  api->handle = handle;
  
  table_key_init(api, name);
  
  return api;
}

static void synth_api_done(synth_api_t* api){
  syslog(LOG_INFO, "Unloading synth plugin (%s)", table_key(api));
  
  table_key_done(api);
  
  if(api->handle){
    dlclose(api->handle);
  }
  
  free(api);
}

void synth_op_init_(synth_op_t* op, synth_t* synth, const char* text, size_t size){
  op->state = SYNTH_OP_INIT;
  op->text = strdup(text);
  op->synth = synth;
  
  rb_dyn_init(&op->buf, size);
  sem_init(&op->buf_sem, 0, 1); /* buffer accessible by default */
  sem_init(&op->owf_sem, 0, 0); /* wait for free space on overflow */
}

void synth_op_done_(synth_op_t* op){
  sem_destroy(&op->owf_sem);
  sem_destroy(&op->buf_sem);
  rb_dyn_done(&op->buf);
  
  free(op->text);
}

static void synth_op_falloff(synth_op_t* op){
  size_t size = rb_size(&op->buf) / sizeof(short);
  
  unsigned sdiv = 7;
  unsigned sval = 1 << sdiv;
  
  for(; sval > size; sval >>= 1, sdiv--);
  size = sval * sizeof(short);
  
  short data[sval];
  short *ptr = data;
  
  rb_pop(&op->buf, (rb_val_t*)data, size);
  
  for(; sval; ptr++){
    *ptr = ((int)*ptr * sval--) >> sdiv;
  }
  
  rb_put(&op->buf, (rb_val_t*)data, size);
}

size_t synth_op_put_block_(synth_op_t* op, const void* data, size_t size){
  if(op->state != SYNTH_OP_PROC){
    return 0;
  }
  
  sem_wait(&op->buf_sem); /* wait until buffer becomes accessible */
  
  LOG("put (%s) render (%s) samples (%u) beg", table_key(op->synth), op->text, (unsigned int)size);
  
  size_t total = 0, chunk;
  const rb_val_t *bptr = data;
  
  for(; op->state == SYNTH_OP_PROC && size; ){
    chunk = rb_put(&op->buf, bptr, size);
    
    if(chunk > 0){ /* data output complete */
      LOG("put (%s) render (%s) samples (%u) block", table_key(op->synth), op->text, (unsigned int)chunk);
      total += chunk;
      bptr += chunk; /* moving forward */
      size -= chunk; /* decrease right */
    }else{ /* has no free space in buffer */
      sem_post(&op->buf_sem); /* make buffer accessible */
      sem_wait(&op->owf_sem); /* wait for free space */
      sem_wait(&op->buf_sem);
    }
  }
  
  sem_post(&op->buf_sem); /* free buffer to access */
  
  return total;
}

size_t synth_op_get(synth_op_t* op, void* data, size_t size){
  sem_wait(&op->buf_sem); /* wait until buffer busy */
  
  //LOG("get (%s) render (%s) bytes (%d) req", table_key(op->synth), op->text, (int)size);
  
  size = rb_get(&op->buf, data, size);
  
  LOG("get (%s) render (%s) bytes (%d)", table_key(op->synth), op->text, (int)size);

  int val = 0;
  sem_getvalue(&op->owf_sem, &val);
  if(val < 1){
    sem_post(&op->owf_sem);
  }
  
  sem_post(&op->buf_sem); /* free buffer to access */
  
  return size;
}

size_t synth_op_len(synth_op_t* op){
  sem_wait(&op->buf_sem); /* wait until buffer busy */
  
  size_t size = op->buf.size;
  
  sem_post(&op->buf_sem); /* free buffer to access */

  return size;
}

void synth_op_begin_(synth_op_t* op){
  LOG("Synth (%s) render (%s) begin", table_key(op->synth), op->text);
  
  op->state = SYNTH_OP_PROC;
}

void synth_op_end_(synth_op_t* op){
  LOG("Synth (%s) render (%s) end", table_key(op->synth), op->text);
  
  if(op->state == SYNTH_OP_STOP){
    sem_wait(&op->buf_sem); /* wait until buffer busy */
    
    synth_op_falloff(op);
    
    sem_post(&op->buf_sem); /* free buffer to access */
  }
  
  op->state = SYNTH_OP_DONE;
}

void synth_op_cancel(synth_op_t* op){
  LOG("Synth (%s) render (%s) cancel", table_key(op->synth), op->text);
  
  op->state = SYNTH_OP_STOP;
  
  sem_post(&op->owf_sem);
}

synth_api_t* _synth_api_pool = NULL;

synth_t* synth_init(const char* name, struct event_base* ev_base, json_object* config){
  if(!json_object_is_type(config, json_type_object)){
    syslog(LOG_ERR, "Synth instance (%s) config error: Value isn't object", name);
    return NULL;
  }
  
  const char* type = NULL;
  json_object_object_foreach(config, key, val){
    if(strcmp(key, "type") == 0){
      type = json_object_get_string(val);
      break;
    }
  }
  
  if(!type){
    syslog(LOG_ERR, "Synth instance (%s) config error: Field type not found", name);
    return NULL;
  }
  
  synth_api_t* api = table_find(_synth_api_pool, type);
  
  if(!api){
    api = table_add(_synth_api_pool, synth_api, type);
    
    if(!api){
      return NULL;
    }
  }
  
  api->usage = 0;
  
  syslog(LOG_INFO, "Initializing synth instance (%s)", name);
  synth_t* synth = synth_api_call(api, init, ev_base, config);
  
  if(!synth){
    syslog(LOG_ERR, "Synth instance (%s) initializaton failed", name);
    return NULL;
  }
  
  table_key_init(synth, name);
  
  synth->api = api;
  synth->api->usage++;
  
  return synth;
}

void synth_done(synth_t* synth){
  synth_api_t* api = synth->api;
  
  table_key_done(synth);
  
  synth_api_call(synth->api, done, synth);

  api->usage--;
  
  if(!api->usage){
    table_del(_synth_api_pool, synth_api, api);
  }
}

synth_op_t* synth_render(synth_t* synth, const char* text, size_t size){
  synth_op_t* op = synth_api_call(synth->api, op_init, synth, text, size);
  
  LOG("Synth (%s) render (%s) init", table_key(synth), text);
  
  return op;
}

void synth_delete(synth_op_t* op){
  synth_api_call(op->synth->api, op_done, op);
}
