#ifndef __MESSAGE_H__
#define __MESSAGE_H__ "message.h"

#include "dbus.h"

typedef struct message_helper message_helper;
typedef int(*message_handler)(DBusConnection *connection, DBusMessage *message, void *userdata);

typedef enum {
  MESSAGE_ANY = 0,
  MESSAGE_METHOD,
  MESSAGE_SIGNAL
} message_type;

typedef enum {
  MESSAGE_NONE = 0,
  MESSAGE_INPUT,
  MESSAGE_OUTPUT
} message_direction;

struct message_helper {
  const char *service;
  message_type type;
  const char *path;
  const char *iface;
  const char *name;
  message_handler handler;
  void *userdata;
};

#define message_helper_init(filter_, service_, type_, path_, iface_, name_, handler_, userdata_) { \
    (filter_)->service = (service_);                                    \
    (filter_)->type = (type_);                                          \
    (filter_)->path = (path_);                                          \
    (filter_)->iface = (iface_);                                        \
    (filter_)->name = (name_);                                          \
    (filter_)->handler = (handler_);                                    \
    (filter_)->userdata = (userdata_);                                  \
  }

int message_helper_attach(message_helper *filter, DBusConnection *connection);
int message_helper_detach(message_helper *filter, DBusConnection *connection);

DBusMessage *message_helper_create(message_helper *filter, const char *destination);
DBusMessage *message_helper_return(message_helper *filter, DBusMessage *request);

#define message_helper_except(connection, request, error, message...) message_helper_output(connection, dbus_message_new_error_printf(request, error, ##message))

int message_helper_output(DBusConnection *connection, DBusMessage *message);

#define message_helper_delete(message) dbus_message_unref(message)

char* message_helper_mknode(const char* name);
char* message_helper_mkpath(const char* root, const char* node);

const char *message_helper_i11n();

void message_helper_i11n_iface(const char *name);

void message_helper_i11n_method(const char *name);
void message_helper_i11n_signal(const char *name);

void message_helper_i11n_arg(const char *name, const char *type, message_direction direction);

#define message_helper_i11n_method_arg message_helper_i11n_arg
#define message_helper_i11n_signal_arg(name, type) message_helper_i11n_arg(name, type, MESSAGE_NONE)

void message_helper_i11n_node(const char *name);

#endif//__MESSAGE_H__
