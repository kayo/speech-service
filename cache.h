#ifdef CACHE_SUPPORT

#ifndef __cache_h__
#define __cache_h__ "cache.h"

typedef struct cache_ cache_t;

typedef void(*cache_cb)(void* data, size_t size, void* ud);

typedef struct cache_put_ cache_put_t;

struct cache_ {
  struct event_base* ev_base;
  char* path;
  size_t refs;
};

void cache_init(cache_t *cache, const char* path, struct event_base* ev_base);
void cache_done(cache_t *cache);

void cache_get(cache_t *cache, const char* name, cache_cb cb, void* ud);
cache_put_t* cache_set(cache_t *cache, const char* name);

void cache_put(cache_put_t *put, void* data, size_t size);

#define cache_add(cache, put, name, data, size) if(!put) put = cache_set(cache, name); cache_put(put, data, size);

#endif//__cache_h__

#else//CACHE_SUPPORT

typedef void cache_t;



#endif//CACHE_SUPPORT
