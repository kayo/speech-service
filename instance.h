#ifndef __instance_h__
#define __instance_h__ "instance.h"

#include "message.h"
#include "table.h"

#include "synth.h"
#include "audio.h"

typedef struct phrase_ phrase_t;
typedef struct sample_ sample_t;

typedef struct instance_ instance_t;

struct instance_ {
  table_cookie;
  
  synth_t* synth;
  audio_stm_t* audio;
  
  phrase_t* phrase;
  synth_op_t* operation;
  
  struct event ev_play;
  struct timeval play_delay;
  size_t play_chunk;
  
  /* dbus interface */
  DBusConnection* dbus_conn;
  char* path;
  
  message_helper introspect;
  message_helper state;
  message_helper queue;
  message_helper shift;
  message_helper deque;
};

instance_t* instance_init(char* name, struct event_base* ev_base, synth_t* synth, audio_stm_t* audio, cache_t* cache);
void instance_done(instance_t* instance);

void instance_dbus_attach(instance_t* instance, DBusConnection* connection);
void instance_dbus_detach(instance_t* instance, DBusConnection* connection);

#endif//__instance_h__
