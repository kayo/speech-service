#ifndef __table_h__
#define __table_h__

#include "chain.h"

#ifdef __table_c__
typedef struct table_data table_t;
#else
typedef void table_t;
#endif

struct table_data {
  chain_cookie;
  char* name;
};

#define table_cookie struct table_data __cookie__

#define table_init(base) chain_init(base)
#define table_done(base) chain_done(base)

#define table_(node, field) (((struct table_data*)(node))->field)

#define table_each(base, node) chain_each(base, node)

#define table_find(base, _key) chain_find(base, node, !strcmp(table_(node, name), _key))

#define table_add(base, type, name, args...) chain_new(base, type, chain_end, name, ##args)

#define table_del(base, type, node, args...) chain_del(base, type, node, ##args)

#define table_key(base) table_(base, name)
#define table_key_init(base, name) table_key(base) = strdup(name)
#define table_key_done(base) free(table_key(base))

#endif//__table_h__
