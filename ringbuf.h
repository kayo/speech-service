#ifndef __ringbuf_h__
#define __ringbuf_h__ "ringbuf.h"

/**
 * @brief Buffer value type
 */
typedef unsigned char rb_val_t;

/**
 * @brief Buffer size type
 */
typedef unsigned int rb_len_t;

#ifdef __ringbuf_c__
typedef struct rb_obj rb_buf_t;
#else
typedef void rb_buf_t;
#endif

/**
 * @brief Dynamic buffer type
 */
typedef struct rb_obj rb_dyn_t;

/**
 * @brief Common buffer structure
 */
struct rb_obj {
  rb_len_t  size; /**< buffer filling level */
  rb_val_t* rptr; /**< reading pointer */
  rb_val_t* wptr; /**< writing pointer */
  rb_val_t* edge; /**< buffer edge to prevent overflow */
  rb_val_t* data; /**< pointer to beginning of buffer */
};

/**
 * @brief Static buffer constructor
 * @param self Target buffer
 * @param size Dynamic buffer size
 */
void rb_dyn_init(rb_buf_t* self, rb_len_t size);

/**
 * @brief Dynamic buffer destructor
 * @param self Target buffer
 */
void rb_dyn_done(rb_buf_t* self);

/**
 * @brief Static buffer definition
 * @param size Static buffer size
 */
#define rb_sta(SIZE) struct {                                       \
    rb_len_t  size; /**< buffer filling level */                    \
    rb_val_t* rptr; /**< reading pointer */                         \
    rb_val_t* wptr; /**< writing pointer */                         \
    rb_val_t* edge; /**< buffer edge to prevent overflow */         \
    rb_val_t  data[SIZE]; /**< pointer to beginning of buffer */    \
  }

/**
 * @brief Static buffer constructor
 * @param self Target buffer
 */
#define rb_sta_init(self) {                             \
    (self)->wptr = (self)->rptr = (self)->data;         \
    (self)->edge = (self)->data + sizeof((self)->data); \
    (self)->size = 0;                                   \
  }

/**
 * @brief Static buffer destructor
 * @param self Target buffer
 */
#define rb_sta_done(self)

/**
 * @brief Get size of used space in buffer
 * @param self Target buffer
 */
rb_len_t rb_size(const rb_buf_t* self);

/**
 * @brief Get size of free space in buffer
 * @param self Target buffer
 */
rb_len_t rb_free(const rb_buf_t* self);

/**
 * @brief Write data to end of buffer
 * @param self Target buffer
 * @param data Buffer where data can be found
 * @param size Size of data to write
 */
rb_len_t rb_put(rb_buf_t* self, const rb_val_t* data, rb_len_t size);

/**
 * @brief Read data from beginning of buffer
 * @param self Target buffer
 * @param data Buffer where data can be placed
 * @param size Size of data to read
 */
rb_len_t rb_get(rb_buf_t* self, rb_val_t* data, rb_len_t size);

/**
 * @brief Read data from end of buffer
 * @param self Target buffer
 * @param data Buffer where data can be placed
 * @param size Size of data to read
 */
rb_len_t rb_pop(rb_buf_t* self, rb_val_t *data, rb_len_t size);

#endif//__ringbuf_h__
