#define __chain_c__

#include <sys/types.h>
#include "chain.h"

size_t chain_size(chain_t *rest){
  size_t size = 0;
  for(chain_t *node = NULL; node ? node != rest : (node = rest) != NULL; node = node->next, size++);
  return size;
}

chain_t* chain_get(chain_t *rest, ssize_t pos){
  chain_t *node = rest;
  if(pos < 0){
    for(; node && pos != 0; node = node->prev, pos++);
  }else{
    for(; node && pos != 0; node = node->next, pos--);
  }
  return node;
}

ssize_t chain_pos(chain_t *rest, chain_t *node){
  if(rest && node){
    for(size_t pos = 0; rest->next; rest = rest->next, pos++){
      if(rest == node){
        return pos;
      }
    }
  }
  return chain_end;
}

chain_t* _chain_put(chain_t *rest, chain_t *node, ssize_t pos){
  if(!node){
    return rest;
  }
  
  if(!rest){
    node->prev = node;
    node->next = node;
    return node;
  }
  
  chain_t *curr = chain_get(rest, pos);
  
  if(pos < 0){ /* insert after curr */
    node->prev = curr;
    node->next = curr->next;
    
    curr->next->prev = node;
    curr->next = node;
  }else{ /* insert before curr */
    node->prev = curr->prev;
    node->next = curr;
    
    curr->prev->next = node;
    curr->prev = node;
  }
  
  if(pos == 0){
    return node;
  }
  
  return rest;
}

chain_t* _chain_out(chain_t *rest, chain_t *node){
  if(!node){
    return rest;
  }
  
  /* no check that node in chain: hardcore mode :) */
  
  if(node == rest){
    rest = node->next;
  }
  
  node->prev->next = node->next;
  node->next->prev = node->prev;
  
  node->next = node->prev = NULL;
  
  if(rest->prev == NULL && rest->next == NULL){
    return NULL;
  }
  
  return rest;
}
