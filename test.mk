test:
	mkdir -p .cache
	_CACHE=.cache $(if $(debug),gdb --args) ./$(project).elf

test.call=mdbus2 $(project) $(1) || true
test.start=$(call test.call,/ org.illumium.speech.Manager.GetInstance $(1) default)
test.queue=$(call test.call,/$(1)_default org.illumium.speech.Speaker.Queue '$(2)')
test.state=$(call test.call,/$(1)_default org.illumium.speech.Speaker.State)
test.shift=$(call test.call,/$(1)_default org.illumium.speech.Speaker.Shift true $(2))

test.rhvoice:
	$(call test.start,russian)
	$(call test.queue,russian,Первая фраза!)
	$(call test.queue,russian,Вторая фраза!)
	$(call test.queue,russian,Третья фраза!)
	for i in `seq 1`; do $(call test.state,russian); sleep 1; done
	$(call test.shift,russian,0)
	$(call test.queue,russian,Четвертая фраза!)

test.status:
	$(call test.state,russian)
