#ifndef __chain_h__
#define __chain_h__ "chain.h"

#include <stddef.h>
#include <sys/types.h>

#ifdef CHAIN_THREAD_SAFE
#  include <pthread.h>
#  define chain_mutex_(name) pthread_mutex_t name##_mutex_
#  define chain_mutex_init_(name) pthread_mutex_init(&(name##_mutex_), NULL)
#  define chain_mutex_done_(name) pthread_mutex_destroy(&(name##_mutex_))

#  define chain_lock(name) pthread_mutex_lock(&(name##_mutex_))
#  define chain_unlock(name) pthread_mutex_unlock(&(name##_mutex_))

#  define chain_mutex_wrap(name, op) ({         \
      chain_lock(name);                         \
      chain_t* ret = op;                        \
      chain_unlock(name);                       \
      ret;                                      \
    })
#endif

#ifndef __chain_c__
typedef void chain_t;
#else//__chain_c__
typedef struct chain_data chain_t;
#endif//__chain_c__

/* 
 * Chain is the ring of node(s)
 *     ___     ___          ___
 *  __|   |___|   |__....__|   |__
 * |  |___|   |___|        |___|  |
 * |_________________...._________|
 *
 */

struct chain_data {
  chain_t* next;
  chain_t* prev;
};

#define chain_cookie struct chain_data __cookie__

#define chain_(node, field) (((struct chain_data*)(node))->field)
#define chain_t(node) typeof(node)
#define chain_s(node) sizeof(chain_t(*(node)))
#define chain_f(type, func, node, args...) type##_##func(node, ##args)

#define chain_def(type, node) type* node
#define chain_init(node) node = NULL
#define chain_done(node)

#ifdef CHAIN_THREAD_SAFE
#  define chain_def_ts(type, node) chain_mutex_(node); chain_def(type, node)
#  define chain_init_ts(node) chain_mutex_init_(node); chain_init(node)
#  define chain_done_ts(node) chain_mutex_done_(node); chain_done(node)
#endif

size_t chain_size(chain_t *node);
#define chain_len(rest) chain_size(rest)

#define chain_beg 0
#define chain_end -1

chain_t* chain_get(chain_t *rest, ssize_t pos);
ssize_t chain_pos(chain_t *rest, chain_t *node);

chain_t* _chain_put(chain_t *rest, chain_t *node, ssize_t pos);
#define chain_put(rest, node, pos) (rest = _chain_put(rest, node, pos))
#define chain_put_ts(rest, node, pos) chain_mutex_wrap(rest, chain_put(rest, node, pos))

chain_t* _chain_out(chain_t *rest, chain_t *node);
#define chain_out(rest, node) (rest = _chain_out(rest, node))
#define chain_out_ts(rest, node) chain_mutex_wrap(rest, chain_out(rest, node))
#define chain_pop(rest, pos) chain_out(rest, chain_get(rest, pos))
#define chain_pop_ts(rest, pos) chain_out_ts(rest, chain_get(rest, pos))

#define chain_new(rest, type, pos, args...) ({          \
      chain_t(rest) node = chain_f(type, init, ##args); \
      if(node) chain_put(rest, node, pos);              \
      node;                                             \
    })
#define chain_new_ts(rest, type, pos, args...) chain_mutex_wrap(rest, chain_new(rest, type, pos, ##args))

#define chain_add(rest, type, args...) chain_new(rest, type, chain_end, ##args)
#define chain_add_ts(rest, type, args...) chain_new_ts(rest, type, chain_end, ##args)

#define chain_del(rest, type, _node_, args...) ({        \
      chain_t(rest) node = (_node_);                     \
      chain_out(rest, node);                             \
      if(node) chain_f(type, done, node, ##args);        \
      NULL;                                              \
    })
#define chain_del_ts(rest, type, _node_, args...) chain_mutex_wrap(rest, chain_del(rest, type, _node_, ##args))

#define chain_deq(rest, type, args...) chain_del(rest, type, rest, ##args)
#define chain_deq_ts(rest, type, args...) chain_del_ts(rest, type, rest, ##args)

#define chain_turn(node, pos) (node = chain_get(node, pos))

#define chain_each(rest, node)                      \
  for(chain_t(rest) node = NULL;                    \
      node ? node != rest : (node = rest) != NULL;  \
      chain_turn(node, 1))

#define chain_with(rest, node, cond)            \
  chain_each(rest, node) if(cond)

#define chain_find(rest, node, cond) ({         \
      chain_t(rest) __found__ = NULL;           \
      chain_with(rest, node, cond){             \
        __found__ = node;                       \
        break;                                  \
      }                                         \
      __found__;                                \
    })

#endif//__chain_h__
