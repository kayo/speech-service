#include "common.h"
#include "festival.h"

#include "chain.h"

#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>

static size_t _escape_phrase(const char* phrase, char** escaped){
  size_t l = 0;
  
  for(const char *c = phrase; *c != '\0'; c++, l++){
    if(*c == '\\' || *c == '"' || *c == '\n'){
      l++;
    }
  }
  
  *escaped = calloc(l, sizeof(phrase[0]));
  
  char *t = *escaped;
  for(const char *c = phrase; *c != '\0'; c++, t++){
    if(*c == '\\' || *c == '"'){
      *t = '\\';
      t++;
    }
    if(*c == '\n'){
      *t = '\\';
      t++;
      *t = 'n';
    }else{
      *t = *c;
    }
  }
  
  return t - *escaped;
}

struct festival_pending_ {
  chain_cookie;

  char* phrase;
  festival_t* festival;
  
  festival_data_cb data_cb;
  void* data_ud;

  char active; /* now in processing */
};

static void festival_pending_init(festival_pending_t* pending, const char* phrase, festival_t* festival, festival_data_cb data_cb, void* data_ud){
  pending->phrase = strdup(phrase);
  pending->festival = festival;
  pending->data_cb = data_cb;
  pending->data_ud = data_ud;
}

static void festival_pending_done(festival_pending_t* pending){
  free(pending->phrase);
}

static void _festival_resume(festival_t* festival);

static void _festival_proc_init(festival_t* festival);

static void _festival_proc_exit(evutil_socket_t fd, short ev, void *ud){
  festival_t* festival = ud;
  
  if(kill(festival->pid, 0) < 0){ /* process is done */
    syslog(LOG_INFO, "Festival process stopped (%s)", table_(festival, name));
    
    event_del(&festival->ev_end);

    festival_pending_t* pending = festival->pending;
    if(pending && pending->active){
      event_del(&festival->ev_out);
      event_del(&festival->ev_err);
      
      pending->active = 0;
    }
    
    if(festival->fd0){
      close(festival->fd0);
      festival->fd0 = 0;
    }
    
    if(festival->fd1){
      close(festival->fd1);
      festival->fd1 = 0;
    }

    if(festival->fd2){
      close(festival->fd2);
      festival->fd2 = 0;
    }
    
    festival->pid = 0;

    if(!festival->done){
      _festival_proc_init(festival);
    }
  }
}

static void _festival_proc_init(festival_t* festival){
  syslog(LOG_INFO, "Starting festival process (%s)", table_(festival, name));
  
  int pipe0[2]; /* stdin */
  int pipe1[2]; /* stdout */
  int pipe2[2]; /* stderr */
  
  if(pipe(pipe0) < 0){
    syslog(LOG_ERR, "Unable to create stdin pipe (%s)", strerror(errno));
    return;
  }

  if(pipe(pipe1) < 0){
    syslog(LOG_ERR, "Unable to create stdout pipe (%s)", strerror(errno));
    return;
  }
  
  if(pipe(pipe2) < 0){
    syslog(LOG_ERR, "Unable to create stderr pipe (%s)", strerror(errno));
    return;
  }
  
  int pid = fork();
  
  if(pid < 0){
    syslog(LOG_ERR, "Unable to create process (%s)", strerror(errno));
    return;
  }
  
  if(pid == 0){ /* subprocess */
    
    if(dup2(pipe0[0], 0) < 0){
      syslog(LOG_ERR, "Unable to redirect stdin (%s)", strerror(errno));
      return;
    }
    close(pipe0[1]);
    
    if(dup2(pipe1[1], 1) < 0){
      syslog(LOG_ERR, "Unable to redirect stdout (%s)", strerror(errno));
      return;
    }
    close(pipe1[0]);

    if(dup2(pipe2[1], 2) < 0){
      syslog(LOG_ERR, "Unable to redirect stderr (%s)", strerror(errno));
      return;
    }
    close(pipe2[0]);
    
    const char* path = getenv("_FESTIVAL");
    if(!path){
      path = "/usr/bin/festival";
    }
    
    char *const argv[] = { "--language", table_(festival, name), NULL };
    char *const envp[] = { NULL };
    
    if(execve(path, argv, envp) < 0){
      syslog(LOG_ERR, "Unable to run process (%s) (%s)", path, strerror(errno));
    }
    
    return;
  }
  
  /* controller */
  syslog(LOG_INFO, "Festival process started (%s)", table_(festival, name));
  
  close(pipe0[0]);
  close(pipe1[1]);
  close(pipe2[1]);
  
  festival->fd0 = pipe0[1];
  festival->fd1 = pipe0[0];
  festival->fd2 = pipe0[0];
  
  festival->pid = pid;
  
  event_assign(&festival->ev_end, festival->ev_base, SIGCHLD, EV_SIGNAL, _festival_proc_exit, festival);
  event_priority_set(&festival->ev_end, 1);
  
  event_add(&festival->ev_end, NULL);
  
  if(festival->pending){
    _festival_resume(festival);
  }
}

static void _festival_proc_done(festival_t* festival){
  if(festival->pid){
    syslog(LOG_INFO, "Stopping festival process (%s)", table_(festival, name));
    
    kill(festival->pid, SIGTERM);
  }
}

static void _festival_proc_data(evutil_socket_t fd, short ev, void *ud);

static void _festival_proc_wait(festival_t* festival, struct timeval* timeout){
  int ev = EV_READ;
  
  if(timeout){
    ev |= EV_TIMEOUT;
  }
  
  event_assign(&festival->ev_out, festival->ev_base, festival->fd1, ev, _festival_proc_data, festival);
  event_priority_set(&festival->ev_out, 0);
  
  event_assign(&festival->ev_err, festival->ev_base, festival->fd2, ev, _festival_proc_data, festival);
  event_priority_set(&festival->ev_err, 0);
  
  event_add(&festival->ev_out, timeout);
  event_add(&festival->ev_err, timeout);
}

static void _festival_proc_data(evutil_socket_t fd, short ev, void *ud){
  festival_t* festival = ud;
  festival_pending_t* pending = festival->pending;

  if(pending && !pending->active){
    return;
  }
  
  int proc_fd = fd == festival->fd1 ? 1 : 2;
  
  if(ev & EV_TIMEOUT){
    event_del(&festival->ev_out);
    event_del(&festival->ev_err);
    
    festival->pending->data_cb(0, NULL, 0, festival->pending->data_ud);
    
    chain_del(festival->pending, festival_pending, pending);
    
    _festival_resume(festival);
    
    return;
  }
  
  if(ev & EV_READ){
    char buffer[1024*64];
    
    for(ssize_t size; (size = read(fd, buffer, sizeof(buffer))) > 0; ){
      festival->pending->data_cb(proc_fd, buffer, size, festival->pending->data_ud);
    }
    
    _festival_proc_wait(festival, &festival->wait_tv);
  }
}

static const char _festival_render_beg[] = "(RenderText \"";
static const char _festival_render_end[] = "\")\n";

static void _festival_resume(festival_t* festival){
  festival_pending_t* pending = festival->pending;
  
  if(pending && !pending->active){
    pending->active = 1;
    
    _festival_proc_wait(festival, NULL);
    
    write(festival->fd0, _festival_render_beg, sizeof(_festival_render_beg) - 1);
    
    char *escaped;
    size_t length = _escape_phrase(pending->phrase, &escaped);
    write(festival->fd0, escaped, length);
    free(escaped);
    
    write(festival->fd0, _festival_render_end, sizeof(_festival_render_end) - 1);
  }
}

festival_pending_t *festival_render(festival_t* festival, const char* phrase, festival_data_cb data_cb, void* data_ud){
  festival_pending_t* pending = chain_new(festival->pending, festival_pending, chain_end, phrase, festival, data_cb, data_ud);
  
  _festival_resume(festival);
  
  return pending;
}

char festival_cancel(festival_pending_t* pending){
  if(pending->active){
    return -1;
  }
  
  chain_del(pending->festival->pending, festival_pending, pending);
  return 0;
}

static const char _festival_render_fun[] =
  "(define (FormatByte byte)"
  " (let ((lo (% byte 256)))"
  "  (format stderr \"%02x%02x\" (/ (- byte low) 256) low)))\n"
  "(define (RenderText text)"
  " (let ((utt (eval (list 'Utterance 'Text text)))"
  "       (wav) (samples) (index 0))"
  "  (utt.synth utt) (set! wav (utt.wave utt))"
  "  (set! samples (cadr (assq 'num_samples (wave.info wav))))"
  "  (if (> 0 samples) (while (< index samples)"
  "   (FormatByte (wave.get wav index 0))"
  "   (set! index (+ 1 index))))))\n";

void festival_init(festival_t* festival, struct event_base* ev_base){
  festival->ev_base = ev_base;
  
  festival->wait_tv.tv_sec = 0;
  festival->wait_tv.tv_usec = 250000;
  
  festival->done = 0;
  
  chain_init(festival->pending);
  
  _festival_proc_init(festival);
  
  if(festival->pid){
    write(festival->fd1, _festival_render_fun, sizeof(_festival_render_fun) - 1);
  }
}

void festival_done(festival_t* festival){
  for(; festival->pending; ){
    chain_del(festival->pending, festival_pending, festival->pending);
  }
  
  chain_done(festival->pending);
  
  festival->done = 1;
  
  _festival_proc_done(festival);
}
