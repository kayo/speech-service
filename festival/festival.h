#ifndef __festival_h__
#define __festival_h__ "festival.h"

#include "table.h"
#include "message.h"

typedef struct festival_ festival_t;

/*
  fd = 1 - stdout
  fd = 2 - stderr
  
  fd = 0 - nodata (timeout reacher)
 */
typedef void(*festival_data_cb)(int fd, void* data, size_t size, void* ud);

typedef struct festival_pending_ festival_pending_t;

struct festival_ {
  table_cookie;
  
  struct event_base* ev_base;
  
  festival_pending_t* pending;
  
  /* sub process */
  int pid;
  
  int fd0; /* stdin */
  int fd1; /* stdout */
  int fd2; /* stderr */
  
  struct event ev_end;
  struct event ev_out;
  struct event ev_err;
  
  struct timeval wait_tv;
  
  char done;
};

void festival_init(festival_t* festival, struct event_base* ev_base);
void festival_done(festival_t* festival);

festival_pending_t* festival_render(festival_t* festival, const char* phrase, festival_data_cb data_cb, void* data_ud);
char festival_cancel(festival_pending_t* pending);

#endif//__festival_h__
