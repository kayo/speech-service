#ifdef PULSE_SUPPORT

#include "common.h"
#include "pulse.h"
#include "audio.h"

#include <string.h>

/* 
 * Pulseaudio Context
 */

static const audio_io_t _pulse_io = {
  /*.init = */pulse_stm_init,
  /*.done = */pulse_stm_done,
  /*.play = */pulse_stm_play
};

static void _pulse_ctx_done(pulse_ctx_t* ctx){
  event_del(&ctx->ev_conn);
  
  if(!ctx->pa_ctx){
    return;
  }
  
  pa_context_state_t state = pa_context_get_state(ctx->pa_ctx);
  if(state != PA_CONTEXT_TERMINATED && state != PA_CONTEXT_FAILED){
    syslog(LOG_INFO, "Disconnecting pulseaudio context (%s)", ctx->client);
    pa_context_disconnect(ctx->pa_ctx);
    return;
  }
  
  pa_context_unref(ctx->pa_ctx);
  ctx->pa_ctx = NULL;
  ctx->pa_st = PA_CONTEXT_UNCONNECTED;
  
  if(ctx->dead){
    syslog(LOG_INFO, "Pulseaudio context (%s) removed", ctx->client);
    
    event_mainloop_api_done(&ctx->ml_api);
    
    if(ctx->server){
      free(ctx->server);
    }
    if(ctx->client){
      free(ctx->client);
    }
    
    free(ctx);
  }
}

const char* _pulse_ctx_error(pulse_ctx_t* ctx){
  return pa_strerror(pa_context_errno(ctx->pa_ctx));
}

static void _pulse_ctx_state_cb(pa_context *c, void *ud) {
  pulse_ctx_t* ctx = ud;
  
  switch (ctx->pa_st = pa_context_get_state(ctx->pa_ctx)){
  case PA_CONTEXT_UNCONNECTED:
  case PA_CONTEXT_CONNECTING:
  case PA_CONTEXT_AUTHORIZING:
  case PA_CONTEXT_SETTING_NAME:
    break;
    
  case PA_CONTEXT_READY:
    syslog(LOG_INFO, "Pulseaudio context (%s) connected", ctx->client);
    break;
    
  case PA_CONTEXT_TERMINATED:
    syslog(LOG_INFO, "Pulseaudio context (%s) disconnected", ctx->client);
    
    _pulse_ctx_done(ctx);
    break;
    
  case PA_CONTEXT_FAILED:
  default:
    syslog(LOG_ERR, "Pulseaudio context (%s) connection failed (%s)", ctx->client, _pulse_ctx_error(ctx));
    
    _pulse_ctx_done(ctx);
    event_add(&ctx->ev_conn, &ctx->conn_delay);
  }
}

static void _pulse_ctx_init(pulse_ctx_t* ctx){
  if(!(ctx->pa_ctx = pa_context_new(&ctx->ml_api, ctx->client))){
    syslog(LOG_ERR, "Pulseaudio context (%s) creation failed", ctx->client);
    return;
  }
  
  pa_context_set_state_callback(ctx->pa_ctx, _pulse_ctx_state_cb, ctx);
  
  syslog(LOG_INFO, "Connecting pulseaudio context (%s)", ctx->client);
  pa_context_connect(ctx->pa_ctx, ctx->server, 0, NULL);
}

static void _pulse_ctx_conn(evutil_socket_t fd, short ev, void *ud){
  pulse_ctx_t *ctx = ud;
  
  _pulse_ctx_init(ctx);
}

pulse_ctx_t* pulse_ctx_init(const char* server, const char* client, struct event_base* ev_base){
  syslog(LOG_INFO, "Creating pulseaudio context (%s)", client);
  
  pulse_ctx_t* ctx = malloc(sizeof(pulse_ctx_t));
  
  ctx->io = _pulse_io;
  
  event_mainloop_api_init(&ctx->ml_api, ev_base, 3, 2, 1);
  
  ctx->server = server && strlen(server) ? strdup(server) : NULL;
  ctx->client = client ? strdup(client) : NULL;
  
  ctx->pa_ctx = NULL;
  ctx->pa_st = PA_CONTEXT_UNCONNECTED;
  ctx->dead = 0;
  
  ctx->conn_delay.tv_sec = 5;
  ctx->conn_delay.tv_usec = 0;
  
  event_assign(&ctx->ev_conn, ev_base, -1, EV_TIMEOUT, _pulse_ctx_conn, ctx);
  event_priority_set(&ctx->ev_conn, 0);
  
  _pulse_ctx_init(ctx);
  
  return ctx;
}

void pulse_ctx_done(pulse_ctx_t* ctx){
  syslog(LOG_INFO, "Removing pulseaudio context (%s)", ctx->client);
  
  ctx->dead = 1;
  _pulse_ctx_done(ctx);
}

/* 
 * Pulseaudio Stream
 */

static void _pulse_stm_stop(pulse_stm_t* stm){
  /*
  if(pa_stream_writable_size(stm->pa_stm)){
    pa_operation_unref(pa_stream_flush(stm->pa_stm, NULL, NULL));
  }
  */
  
  /*
  if(!pa_stream_is_corked(stm->pa_stm)){
    pa_operation_unref(pa_stream_cork(stm->pa_stm, 1, NULL, NULL));
  }
  */
  
  if(!stm->stop){
    LOG("Pulseaudio stream (%s) stop", stm->stream);
    stm->stop = 1;
  }
}

static void _pulse_stm_start(pulse_stm_t* stm){
  /*
  if(pa_stream_is_corked(stm->pa_stm)){
    pa_operation_unref(pa_stream_cork(stm->pa_stm, 0, NULL, NULL));
  }
  */
  
  if(stm->stop){
    LOG("Pulseaudio stream (%s) start", stm->stream);
    stm->stop = 0;
    //pa_operation_unref(pa_stream_trigger(stm->pa_stm, NULL, NULL));
  }
}

static void _pulse_stm_done(pulse_stm_t* stm){
  event_del(&stm->ev_conn);
  
  if(!stm->pa_stm){
    return;
  }
  
  if(stm->pa_st != PA_STREAM_TERMINATED && stm->pa_st != PA_STREAM_FAILED){
    syslog(LOG_INFO, "Disconnecting pulseaudio stream (%s)", stm->stream);
    pa_stream_disconnect(stm->pa_stm);
    return;
  }
  
  pa_stream_unref(stm->pa_stm);
  stm->pa_stm = NULL;
  stm->pa_st = PA_STREAM_UNCONNECTED;
  
  if(stm->dead){
    syslog(LOG_INFO, "Pulseaudio stream (%s) removed", stm->stream);

    if(stm->stream){
      free(stm->stream);
    }
    if(stm->device){
      free(stm->device);
    }
    
    free(stm);
  }
}

static void _pulse_stm_state_cb(pa_stream *s, void *ud){
  pulse_stm_t* stm = ud;
  
  switch(stm->pa_st = pa_stream_get_state(stm->pa_stm)){
  case PA_STREAM_UNCONNECTED:
  case PA_STREAM_CREATING:
    break;
    
  case PA_STREAM_TERMINATED:
    syslog(LOG_INFO, "Pulseaudio stream (%s) disconnected", stm->stream);
    
    _pulse_stm_done(stm);
    break;
    
  case PA_STREAM_READY:
    syslog(LOG_INFO, "Pulseaudio stream (%s) connected", stm->stream);
    
    break;
    
  case PA_STREAM_FAILED:
    syslog(LOG_ERR, "Pulseaudio stream (%s) failed (%s)", stm->stream, _pulse_ctx_error(stm->ctx));
    
    _pulse_stm_done(stm);
    event_add(&stm->ev_conn, &stm->conn_delay);
    break;
  }
}

static void _pulse_stm_underflow_cb(pa_stream *s, void *ud){
  pulse_stm_t* stm = ud;
  
  _pulse_stm_stop(stm);
}

/*
static void _pulse_stm_write_cb(pa_stream *s, size_t l, void *ud) {
  
}
*/

static void _pulse_stm_init(pulse_stm_t* stm){
  if(stm->ctx->pa_st != PA_CONTEXT_READY){
    event_add(&stm->ev_conn, &stm->conn_delay);
    return;
  }
  
  if(!(stm->pa_stm = pa_stream_new(stm->ctx->pa_ctx, stm->stream, &stm->pa_ss, NULL))){
    syslog(LOG_INFO, "Pulseaudio stream (%s) creation failed", stm->stream);
    return;
  }
  
  pa_stream_set_state_callback(stm->pa_stm, _pulse_stm_state_cb, stm);
  /*pa_stream_set_write_callback(stm->pa_stm, _pulse_stm_write_cb, stm);*/
  pa_stream_set_underflow_callback(stm->pa_stm, _pulse_stm_underflow_cb, stm);
  
  syslog(LOG_INFO, "Connecting pulseaudio stream (%s)", stm->stream);
  pa_stream_connect_playback(stm->pa_stm, stm->device, &stm->pa_ba, stm->pa_flg, NULL, NULL);
}

static void _pulse_stm_conn(evutil_socket_t fd, short ev, void *ud){
  pulse_stm_t *stm = ud;
  
  _pulse_stm_init(stm);
}

pulse_stm_t* pulse_stm_init(pulse_ctx_t* ctx, unsigned int rate, const char* device, const char* stream){
  syslog(LOG_INFO, "Creating pulseaudio stream (%s)", stream);
  
  pulse_stm_t* stm = malloc(sizeof(pulse_stm_t));
  
  stm->dead = 0;
  stm->stop = 1;
  
  stm->ctx = ctx;
  stm->pa_stm = NULL;
  stm->pa_st = PA_STREAM_UNCONNECTED;
  
  stm->pa_ss.format = PA_SAMPLE_S16LE;
  stm->pa_ss.rate = rate;
  stm->pa_ss.channels = 1;
  
  stm->pa_ba.maxlength = (uint32_t)-1;
  stm->pa_ba.tlength = (uint32_t)-1;
  stm->pa_ba.prebuf = (uint32_t)-1;
  stm->pa_ba.minreq = (uint32_t)-1;
  stm->pa_ba.fragsize = (uint32_t)-1;

  stm->pa_flg = PA_STREAM_NOFLAGS;
  
#ifdef PULSE_LATENCY
  stm->pa_ba.tlength = pa_usec_to_bytes(PULSE_LATENCY, &stm->pa_ss);
  stm->pa_flg |= PA_STREAM_ADJUST_LATENCY;
#endif
  
  /*stm->pa_flg |= PA_STREAM_START_CORKED*/;
  
  stm->device = device && strlen(device) ? strdup(device) : NULL;
  stm->stream = stream ? strdup(stream) : NULL;
  
  stm->conn_delay.tv_sec = 0;
  stm->conn_delay.tv_usec = 500;
  
  event_assign(&stm->ev_conn, event_mainloop_api_event_base(&stm->ctx->ml_api), -1, EV_TIMEOUT, _pulse_stm_conn, stm);
  event_priority_set(&stm->ev_conn, 0);
  
  _pulse_stm_init(stm);
  
  return stm;
}

void pulse_stm_done(pulse_stm_t* stm){
  syslog(LOG_INFO, "Removing pulseaudio stream (%s)", stm->stream);
  
  //pulse_stm_out(stm, NULL, 0);
  
  stm->dead = 1;
  _pulse_stm_done(stm);
}

size_t pulse_stm_play(pulse_stm_t* stm, void* data, size_t size){
  if(stm->pa_st != PA_STREAM_READY){
    // emulate skip
    return size;
  }

  if(!data){
    _pulse_stm_stop(stm);
    
    return 0;
  }
  
  void* buffer;
  
  pa_stream_begin_write(stm->pa_stm, &buffer, &size);
  
  if(size > 0){
    //LOG("Pulseaudio stream (%s) out (%d)", stm->stream, (int)size);
    
    memcpy(buffer, data, size);
    pa_stream_write(stm->pa_stm, buffer, size, NULL, 0, PA_SEEK_RELATIVE);
    
    _pulse_stm_start(stm);
  }
  
  return size;
}

#endif//PULSE_SUPPORT
