ifeq ($(systemd.watchdog),on)
  def+=WATCHDOG_SUPPORT=SYSTEMD
  systemd.daemon?=on
endif

ifeq ($(systemd.daemon),on)
  pkg.deps+=libsystemd-daemon
endif
