ifeq ($(audio.backend),alsa)
  def+=ALSA_SUPPORT=1
  def+=ALSA_LATENCY=$(audio.latency)
  pkg.deps+=alsa
endif
