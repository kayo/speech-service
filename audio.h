#ifndef __audio_h__
#define __audio_h__ "audio.h"

typedef struct audio_io_ audio_io_t;
typedef struct audio_ctx_ audio_ctx_t;
typedef struct audio_stm_ audio_stm_t;
typedef enum audio_req_ audio_req_t;

typedef audio_stm_t* audio_stm_init_f(audio_ctx_t* ctx, unsigned int rate, const char* device, const char* stream);
typedef void audio_stm_done_f(audio_stm_t* stm);
typedef size_t audio_stm_req_f(audio_stm_t* stm, audio_req_t req);
typedef void audio_stm_out_f(audio_stm_t* stm, void* data, size_t size);

struct audio_io_ {
  audio_stm_init_f *init;
  audio_stm_done_f *done;
  audio_stm_req_f *req;
  audio_stm_out_f *out;
};

#define audio_io(pfx) static const audio_io_t pfx##_io = {  \
    /*.init = */(audio_stm_init_f*)pfx##_stm_init,          \
    /*.done = */(audio_stm_done_f*)pfx##_stm_done,          \
    /*.req = */(audio_stm_req_f*)pfx##_stm_req,             \
    /*.out = */(audio_stm_out_f*)pfx##_stm_out,             \
  };

struct audio_ctx_ {
  const audio_io_t *io;
};

struct audio_stm_ {
  audio_ctx_t *ctx;
};

enum audio_req_ {
  audio_play,
  audio_stop,
  audio_drop,
};

static inline audio_stm_t* audio_stm_init(audio_ctx_t* ctx, unsigned int rate, const char* device, const char* stream){
  return ctx->io->init(ctx, rate, device, stream);
}

static inline void audio_stm_done(audio_stm_t* stm){
  stm->ctx->io->done(stm);
}

static inline size_t audio_stm_req(audio_stm_t* stm, audio_req_t req){
  return stm->ctx->io->req(stm, req);
}

static inline void audio_stm_out(audio_stm_t* stm, void* data, size_t size){
  stm->ctx->io->out(stm, data, size);
}

#endif
