ld.lib+=dl
ld.flag+=-Wl,-E

all: plugins
clean: clean-plugins

plugins: $(addprefix plugin.,$(plugins))
clean-plugins: $(addprefix clean-plugin.,$(plugins))

plugin.%:
	$(MAKE) -C $* $(if $(debug),debug=1)
	ln -sf $*/$(call ld.plugin,$*) $(call ld.plugin,$*)

clean-plugin.%:
	$(MAKE) -C $* clean
