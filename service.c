#include "common.h"
#include "service.h"

#include <signal.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

#ifdef WATCHDOG_SUPPORT
#if WATCHDOG_SUPPORT == SYSTEMD
#include <systemd/sd-daemon.h>
#endif
#endif

static int _service_dbus_introspect(DBusConnection *connection, DBusMessage *message, void *ud){
  service_t *service = ud;
  
  DBusMessage *reply = message_helper_return(&service->introspect, message);
  
  message_helper_i11n_iface(DBUS_INTERFACE_INTROSPECTABLE);
  
  message_helper_i11n_method(service->introspect.name);
  message_helper_i11n_method_arg("xml", "s", MESSAGE_OUTPUT);
  message_helper_i11n_method(NULL);
  
  message_helper_i11n_iface(NULL);

  message_helper_i11n_iface(DBUS_INTERFACE_MANAGER);
  
  message_helper_i11n_method(service->get_instance.name);
  message_helper_i11n_method_arg("preset", "s", MESSAGE_INPUT);
  message_helper_i11n_method_arg("device", "s", MESSAGE_INPUT);
  message_helper_i11n_method_arg("instance", "o", MESSAGE_OUTPUT);
  message_helper_i11n_method(NULL);
  
  message_helper_i11n_iface(NULL);
  
  table_each(service->instance, instance){
    message_helper_i11n_node(instance->path + 1);
  }
  
  const char *i11n = message_helper_i11n();
  
  dbus_message_append_args(reply, DBUS_TYPE_STRING, &i11n, DBUS_TYPE_INVALID);
  message_helper_output(connection, reply);
  
  return 1;
}

static char* _service_instance_name(const char* preset, const char* device){
  size_t preset_l = strlen(preset);
  size_t device_l = strlen(device);
  
  char *name = malloc(preset_l + 1 + device_l + 1);

  strcpy(name, preset);
  name[preset_l] = ':';

  strcpy(name + preset_l + 1, device);
  return name;
}

static instance_t* _service_get_instance(service_t* service, const char* preset, const char* device){
  char* name = _service_instance_name(preset, device);
  
  instance_t* instance = table_find(service->instance, name);
  
  if(instance){
    free(name);
    return instance;
  }
  
  synth_t *synth = table_find(service->synth, preset);

  if(!synth){
    return NULL;
  }
  
  audio_stm_t* audio = NULL;
  
#ifdef PULSE_SUPPORT
  if(!audio){
    audio = audio_stm_init(service->pulse, synth->rate, device, name);
  }
#endif

#ifdef ALSA_SUPPORT
  if(!audio){
    audio = audio_stm_init(&alsap_ctx, synth->rate, device, name);
  }
#endif
  
  if(!audio){
    return NULL;
  }
  
#ifdef CACHE_SUPPORT
  instance = table_add(service->instance, instance, name, service->ev_base, synth, audio, &service->cache);
#else
  instance = table_add(service->instance, instance, name, service->ev_base, synth, audio, NULL);
#endif
  
  instance_dbus_attach(instance, service->dbus.conn);
  
  return instance;
}

static int _service_dbus_get_instance(DBusConnection *connection, DBusMessage *message, void *ud){
  service_t *service = ud;

  const char* language = NULL;
  const char* device = NULL;
  
  dbus_message_get_args(message, NULL, DBUS_TYPE_STRING, &language, DBUS_TYPE_STRING, &device, DBUS_TYPE_INVALID);

  if(!language){
    language = "default";
  }
  
  if(!device){
    device = "default";
  }
  
  instance_t* instance = _service_get_instance(service, language, device);
  
  if(!instance){
    message_helper_except(connection, message, DBUS_ERROR_INSTANCE_UNAVAILABLE, "Instance unavailable");
    return 1;
  }
  
  DBusMessage *reply = message_helper_return(&service->get_instance, message);
  
  dbus_message_append_args(reply, DBUS_TYPE_OBJECT_PATH, &instance->path, DBUS_TYPE_INVALID);
  
  message_helper_output(connection, reply);
  
  return 1;
}

static void _service_dbus_bind(event_dbus_t* edb, void* ud){
  service_t* service = ud;
  
  message_helper_attach(&service->introspect, service->dbus.conn);
  message_helper_attach(&service->get_instance, service->dbus.conn);
  
  /*
  table_each(service->synth, festival, synth){
    festival_dbus_attach(synth, service->dbus.conn);
  }
  table_each(service->audio, pulseout, audio){
    pulseout_dbus_attach(audio, service->dbus.conn);
  }
  */
  table_each(service->instance, instance){
    instance_dbus_attach(instance, service->dbus.conn);
  }
}

static void _service_config(service_t* service, const char* file){
  if(!file){
    file = "config.json";
  }
  
  int fd = open(file, O_RDONLY);
  if(fd < 0){
    syslog(LOG_ERR, "Unable to open config (%s)", file);
    return;
  }
  
  struct stat fs;
  fstat(fd, &fs);

  if(fs.st_size == 0){
    close(fd);
    syslog(LOG_ERR, "Config file (%s) is empty", file);
    return;
  }
  
  char *data = mmap(0, fs.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
  
  if(data == MAP_FAILED){
    close(fd);
    syslog(LOG_ERR, "Unable to read file (%s)", file);
    return;
  }
  
  enum json_tokener_error jerr;
  json_object *root = json_tokener_parse_verbose(data, &jerr);
  
  munmap(data, fs.st_size);
  close(fd);

  if(jerr != json_tokener_success){
    syslog(LOG_ERR, "Unable to parse config (%s) (%s)", file, json_tokener_error_desc(jerr));
    return;
  }
  
  json_object_object_foreach(root, name, opts){
    table_add(service->synth, synth, name, service->ev_base, opts);
  }
}

#ifdef WATCHDOG_SUPPORT
static void _service_wdcb(evutil_socket_t fd, short what, void *ud){
  //service_t *service = ud;
  
#if WATCHDOG_SUPPORT == SYSTEMD
  sd_notify(0, "WATCHDOG=1");
#endif
}
#endif

static void _service_exit(evutil_socket_t fd, short what, void *ud){
  service_t *service = ud;
  
  syslog(LOG_INFO, "Stopping");
  
  /* Done components */
  if(service->dbus.conn){
    table_each(service->instance, instance){
      instance_dbus_detach(instance, service->dbus.conn);
    }
    /*
    table_each(service->synth, festival, synth){
      festival_dbus_detach(synth, service->dbus.conn);
    }
    table_each(service->audio, pulseout, audio){
      pulseout_dbus_detach(audio, service->dbus.conn);
    }
    */
    
    message_helper_detach(&service->get_instance, service->dbus.conn);
    message_helper_detach(&service->introspect, service->dbus.conn);
  }
  
  //table_each(service->instance, instance){
  //  instance_done(instance);
  //}

  for(; service->instance; ){
    table_del(service->instance, instance, service->instance);
  }
  
  for(; service->synth; ){
    table_del(service->synth, synth, service->synth);
  }
  
#ifdef PULSE_SUPPORT
  pulse_ctx_done(service->pulse);
#endif
  
  /* Finalizing DBus */
  event_dbus_done(&service->dbus);
  
  /* Deactivate signal handlers */
  event_del(&service->ev_term);
  event_del(&service->ev_intr);
  
#ifdef WATCHDOG_SUPPORT
  event_del(&service->ev_wdtt);
#endif
}

void service_init(service_t* service){
  syslog(LOG_INFO, "Starting");
  
  /* Init event loop */
  
  service->ev_base = event_base_new();
  
  event_base_priority_init(service->ev_base, 5);
  
  /* Init signal handlers */
  
  event_assign(&service->ev_term, service->ev_base, SIGTERM, EV_SIGNAL | EV_PERSIST, _service_exit, service);
  event_priority_set(&service->ev_term, 4);

  event_assign(&service->ev_intr, service->ev_base, SIGINT, EV_SIGNAL | EV_PERSIST, _service_exit, service);
  event_priority_set(&service->ev_intr, 4);

#ifdef WATCHDOG_SUPPORT
  event_assign(&service->ev_wdtt, service->ev_base, -1, EV_TIMEOUT | EV_PERSIST, _service_wdcb, service);
  event_priority_set(&service->ev_wdtt, 4);
#endif
  
  /* Activate signal handlers */
  
  event_add(&service->ev_term, NULL);
  event_add(&service->ev_intr, NULL);

#ifdef WATCHDOG_SUPPORT
  /* Activate watchdog */
#if WATCHDOG_SUPPORT == SYSTEMD
  const char *wd_usec = getenv("WATCHDOG_USEC");
#endif
  if(wd_usec){
    unsigned usec = strtoul(wd_usec, NULL, 10);
    if(usec > 0){
      usec >>= 1; /* need half delay to correct operation */
      syslog(LOG_INFO, "Setup watchdog notifications every %d usec", usec);
      service->wd_delay.tv_sec = usec / 1000000;
      service->wd_delay.tv_usec = usec % 1000000;
      event_add(&service->ev_wdtt, &service->wd_delay);
    }
  }
#endif
  
  /* Initialize tables */
#ifdef CACHE_SUPPORT
  cache_init(&service->cache, getenv("_CACHE"), service->ev_base);
#endif
  
  table_init(service->instance);
  table_init(service->synth);
  
#ifdef PULSE_SUPPORT
  service->pulse = pulse_ctx_init(getenv("_PULSE"), DBUS_SERVICE_SPEECH, service->ev_base);
#endif
  
  /* Initializing DBus */
  
  message_helper_init(&service->introspect,
                      DBUS_SERVICE_SPEECH,
                      MESSAGE_METHOD,
                      "/",
                      DBUS_INTERFACE_INTROSPECTABLE,
                      "Introspect",
                      _service_dbus_introspect,
                      service);
  
  message_helper_init(&service->get_instance,
                      DBUS_SERVICE_SPEECH,
                      MESSAGE_METHOD,
                      "/",
                      DBUS_INTERFACE_MANAGER,
                      "GetInstance",
                      _service_dbus_get_instance,
                      service);
  
  event_dbus_init(&service->dbus,
                  getenv("_BUS"), DBUS_SERVICE_SPEECH,
                  service->ev_base, 0,
                  _service_dbus_bind, NULL, NULL,
                  service);

  _service_config(service, getenv("_CONFIG"));
}

void service_done(service_t* service){
  event_base_free(service->ev_base);
  
  table_done(service->synth);
  table_done(service->instance);
  
  syslog(LOG_INFO, "Stopped");
}

void service_loop(service_t* service){
  syslog(LOG_INFO, "Started");
  
  event_base_loop(service->ev_base, 0);
}
