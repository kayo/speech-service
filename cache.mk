ifeq ($(cache),on)
  cache.chunk?=1024*64
  def+=CACHE_SUPPORT=1
  def+=CACHE_CHUNK=$(cache.chunk)
  pkg.deps+=libcrypto
endif
