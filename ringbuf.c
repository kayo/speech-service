#define __ringbuf_c__ "ringbuf.c"

#include "ringbuf.h"

#include <stdlib.h>

void rb_dyn_init(rb_buf_t *self, rb_len_t size){
  self->wptr = self->rptr = self->data = calloc(size, sizeof(rb_val_t));
  self->edge = self->data + size;
  self->size = 0;
}

void rb_dyn_done(rb_buf_t *self){
  free(self->data);
}

rb_len_t rb_size(const rb_buf_t* self){
  return self->edge - self->data;
}

rb_len_t rb_free(const rb_buf_t* self){
  return rb_size(self) - self->size;
}

rb_len_t rb_put(rb_buf_t* self, const rb_val_t *data, rb_len_t size){
  rb_len_t dlen = rb_free(self);
  if(size > dlen){
    size = dlen;
  }else{
    dlen = size;
  }
  for(; size--; self->size++){    /* we have a free space */
    *self->wptr++ = *data++;
    if(self->wptr == self->edge){ /* edge detected */
      self->wptr = self->data;    /* restart */
    }
  }
  return dlen;
}

rb_len_t rb_get(rb_buf_t* self, rb_val_t *data, rb_len_t size){
  rb_len_t dlen = self->size;
  if(size > dlen){
    size = dlen;
  }else{
    dlen = size;
  }
  for(; size--; self->size--){    /* we have a data */
    *data++ = *self->rptr++;
    if(self->rptr == self->edge){ /* edge detected */
      self->rptr = self->data;    /* restart */
    }
  }
  return dlen;
}

rb_len_t rb_pop(rb_buf_t* self, rb_val_t *data, rb_len_t size){
  rb_len_t dlen = self->size;
  if(size > dlen){
    size = dlen;
  }else{
    dlen = size;
  }
  data += size;
  for(; size--; self->size--){    /* we have a data */
    if(self->wptr == self->data){ /* on beginning of buffer */
      self->wptr = self->edge;    /* back to end */
    }
    *--data = *--self->wptr;
  }
  return dlen;
}
