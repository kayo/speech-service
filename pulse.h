#ifdef PULSE_SUPPORT

#ifndef __pulse_h__
#define __pulse_h__ "pulse.h"

#include "event-mainloop.h"

typedef struct pulse_ctx_ pulse_ctx_t;
typedef struct pulse_stm_ pulse_stm_t;

struct pulse_ctx_ {
  audio_io_t *io;
  
  pa_mainloop_api ml_api;
  pa_context* pa_ctx;
  pa_context_state_t pa_st;
  
  struct event ev_conn;
  struct timeval conn_delay;
  
  char* server;
  char* client;
  
  int dead:1;
};

pulse_ctx_t* pulse_ctx_init(const char* server, const char* client, struct event_base* ev_base);
void pulse_ctx_done(pulse_ctx_t* ctx);

typedef void(*pulse_stm_cb)(void* data, size_t size, void* ud);

struct pulse_stm_ {
  pulse_ctx_t* ctx;
  
  pa_sample_spec pa_ss;
  pa_buffer_attr pa_ba;
  pa_stream* pa_stm;
  pa_stream_state_t pa_st;
  pa_stream_flags_t pa_flg;
  
  char* device;
  char* stream;
  
  struct event ev_conn;
  struct timeval conn_delay;
  
  int stop:1;
  int dead:1;
};

pulse_stm_t* pulse_stm_init(pulse_ctx_t* ctx, unsigned int rate, const char* device, const char* stream);
size_t pulse_stm_play(pulse_stm_t* stm, void* data, size_t size);
void pulse_stm_done(pulse_stm_t* stm);

#endif//__pulse_h__

#endif//PULSE_SUPPORT
