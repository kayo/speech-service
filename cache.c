#ifdef CACHE_SUPPORT

#include "common.h"
#include "cache.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/md5.h>

void cache_init(cache_t *cache, const char* path, struct event_base* ev_base){
  cache->ev_base = ev_base;
  cache->path = path ? strdup(path) : NULL;
  cache->refs = 0;
}

void cache_done(cache_t *cache){
  if(cache->path){
    free(cache->path);
    cache->path = NULL;
  }
}

static inline char _byte_to_char(unsigned char byte){
  return (byte < 10 ? '0' : 'a') + byte;
}

static void _digest_to_string(char* str, unsigned char *dig, size_t len){
  unsigned char* end = dig + len;
  
  for(; dig < end; ){
    *str++ = _byte_to_char(*dig >> 4);
    *str++ = _byte_to_char(*dig++ & 0xf);
  }
}

static char* _cache_file(const char* path, const char* name){
  size_t path_l = strlen(path);

  char* ret = malloc(path_l + 1 + MD5_DIGEST_LENGTH * 2 + 1);
  
  strncpy(ret, path, path_l);
  ret[path_l] = '/';
  
  size_t name_l = strlen(name);
  
  MD5_CTX ctx;
  
  MD5_Init(&ctx);
  MD5_Update(&ctx, name, name_l);

  unsigned char hash[MD5_DIGEST_LENGTH];
  MD5_Final(hash, &ctx);
  
  _digest_to_string(&ret[path_l + 1], hash, MD5_DIGEST_LENGTH);
  
  return ret;
}

typedef struct cache_get_ cache_get_t;

struct cache_get_ {
  cache_t* cache;
  char* file;
  
  struct event ev;
  
  cache_cb cb;
  void* ud;
};

static void _cache_get(evutil_socket_t fd, short ev, void *ud){
  cache_get_t* get = ud;
  
  if(ev & EV_READ){
    ssize_t size = CACHE_CHUNK;
    void* data = malloc(size);
    
    size = read(fd, data, size);
    
    if(size < 0){
      get->cb(NULL, 0, get->ud);
      
      event_del(&get->ev);
      close(fd);
      
      get->cache->refs--;

      free(get->file);
      free(get);
      
      return;
    }
    
    get->cb(data, size, get->ud);
  }
}

void cache_get(cache_t *cache, const char* name, cache_cb cb, void* ud){
  if(!cb){
    return;
  }
  
  if(!cache->path){
    cb(NULL, 0, ud);
  }
  
  char* file = _cache_file(cache->path, name);
  
  int fd = open(file, O_RDONLY);
  
  if(fd < 0 && cb){
    free(file);
    cb(NULL, 0, ud);
    return;
  }
  
  cache_get_t* get = malloc(sizeof(cache_get_t));
  
  get->cache = cache;
  get->file = file;
  
  event_assign(&get->ev, cache->ev_base, fd, EV_READ | EV_PERSIST, _cache_get, get);
  event_priority_set(&get->ev, 0);
  
  cache->refs++;
}

struct cache_put_ {
  cache_t* cache;
  char* file;
  
  int fd;
};

void cache_put(cache_put_t* put, void* data, size_t size){
  if(put){
    if(put->fd){
      if(data){
        write(put->fd, data, size);
      }else{
        put->cache->refs--;
        close(put->fd);
      }
    }
    if(!data){
      if(put->file){
        free(put->file);
      }
      free(put);
    }
  }
}

cache_put_t* cache_set(cache_t *cache, const char* name){
  cache_put_t* put = malloc(sizeof(put));
  
  put->cache = cache;
  put->file = cache->path ? _cache_file(cache->path, name) : NULL;
  put->fd = 0;
  
  if(!put->file){
    return put;
  }
  
  put->fd = open(put->file, O_WRONLY);
  
  if(put->fd < 0){
    put->fd = 0;
    syslog(LOG_WARNING, "Unable to open cache (%s) for writing", put->file);
    return put;
  }
  
  cache->refs++;
  
  return put;
}

#if 0

struct cache_get_pend {
  cache_t* cache;
  struct event ev;
  
  char* file;
  size_t rlen;
  
  void* data;
  size_t size;
  size_t offs;
  
  cache_get_cb cb;
  void* ud;
};

static void _cache_get_done(struct cache_get_pend* pend, evutil_socket_t fd, char ok){
  pend->cache->refs--;
  
  close(fd);
  event_del(&pend->ev);
  
  if(!ok){
    syslog(LOG_ERR, "Unable to read cache (%s)", pend->file);
  }
  pend->cb(ok ? pend->data : NULL, ok ? pend->size : 0, pend->ud);
  
  free(pend->file);
  if(!ok){
    free(pend->data);
  }
  free(pend);
}

static void _cache_get_data(evutil_socket_t fd, short ev, void *ud){
  struct cache_get_pend* pend = ud;
  
  if(ev & EV_READ){
    ssize_t rlen = read(fd, pend->data + pend->offs, pend->rlen);

    if(rlen <= 0){ /* error occured */
      _cache_get_done(pend, fd, 0);
      return;
    }
    
    pend->offs += rlen;
    
    if(pend->offs == pend->size){ /* complete reading */
      _cache_get_done(pend, fd, 1);
      return;
    }
  }
}

void cache_get(cache_t *cache, const char* name, cache_get_cb cb, void* ud){
  if(!cb){
    return;
  }

  if(!cache->path){
    cb(NULL, 0, ud);
  }
  
  char* file = _cache_file(cache->path, name);
  
  int fd = open(file, O_RDONLY | O_NONBLOCK);
  
  if(fd < 0 && cb){
    free(file);
    cb(NULL, 0, ud);
    return;
  }
  
  struct cache_get_pend* pend = malloc(sizeof(struct cache_get_pend));
  
  pend->cache = cache;
  pend->file = file;
  
  struct stat st;
  fstat(fd, &st);
  
  pend->rlen = st.st_blksize;
  
  pend->size = st.st_size;
  pend->data = malloc(st.st_size);
  
  event_assign(&pend->ev, cache->ev_base, fd, EV_READ | EV_PERSIST, _cache_get_data, pend);
  event_priority_set(&pend->ev, 0);

  cache->refs++;
}

struct cache_set_pend {
  cache_t* cache;
  struct event ev;
  
  char* file;
  size_t wlen;
  
  void* data;
  size_t size;
  size_t offs;
  
  cache_set_cb cb;
  void* ud;
};

static void _cache_set_done(struct cache_set_pend* pend, evutil_socket_t fd, char ok){
  pend->cache->refs--;
  
  close(fd);
  event_del(&pend->ev);
  
  if(!ok){
    syslog(LOG_ERR, "Unable to write cache (%s)", pend->file);
  }
  pend->cb(ok, pend->ud);
  
  free(pend->file);
  free(pend);
}

static void _cache_set_data(evutil_socket_t fd, short ev, void *ud){
  struct cache_set_pend* pend = ud;
  
  if(ev & EV_WRITE){
    ssize_t wlen = write(fd, pend->data + pend->offs, pend->wlen);
    
    if(wlen <= 0){ /* error occured */
      _cache_set_done(pend, fd, 0);
      return;
    }
    
    pend->offs += wlen;
    
    if(pend->offs == pend->size){ /* complete reading */
      _cache_set_done(pend, fd, 1);
      return;
    }
  }
}

void cache_set(cache_t *cache, const char* name, void* data, size_t size, cache_set_cb cb, void* ud){
  if(!cb){
    return;
  }
  
  if(!cache->path){
    cb(0, ud);
  }
  
  char* file = _cache_file(cache->path, name);
  
  int fd = open(file, O_WRONLY | O_NONBLOCK);
  
  if(fd < 0 && cb){
    syslog(LOG_WARNING, "Unable to open cache (%s) for writing", file);
    free(file);
    cb(0, ud);
    return;
  }
  
  struct cache_set_pend* pend = malloc(sizeof(struct cache_set_pend));
  
  pend->cache = cache;
  pend->file = file;
  
  pend->wlen = 64 * 1024;
  pend->size = size;
  pend->data = data;

  pend->cb = cb;
  pend->ud = ud;
  
  event_assign(&pend->ev, cache->ev_base, fd, EV_WRITE | EV_PERSIST, _cache_set_data, pend);
  event_priority_set(&pend->ev, 0);
  
  cache->refs++;
}
#endif

#endif//CACHE_SUPPORT
