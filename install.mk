bin.dir?=/usr/sbin
mod.dir?=/usr/lib
cfg.dir?=/etc
dbus.conf.dir?=/etc/dbus-1/system.d
systemd.conf.dir?=/etc/systemd/system

install:
	getent group speech-access || groupadd -r speech-access
	systemctl stop $(project) || true
	cp $(project).elf $(bin.dir)/$(project)
ifneq ($(plugins),)
	mkdir -p $(mod.dir)/$(project)
	cp $(foreach p,$(plugins),$(p)/$(call ld.plugin,$(p))) $(mod.dir)/$(project)
endif
	cp config.json $(cfg.dir)/$(project).json
	cp $(project).conf $(dbus.conf.dir)
	systemctl reload dbus
	cp $(project).service $(systemd.conf.dir)
	systemctl --system daemon-reload
	systemctl enable $(project)
	systemctl start $(project)

uninstall:
	systemctl stop $(project)
	systemctl disable $(project)
	rm -f $(bin.dir)/$(project)
	rm -rf $(mod.dir)/$(project)
	rm -f $(cfg.dir)/$(project).json
	rm -f $(dbus.conf.dir)/$(project).conf
	rm -f $(system.service.dir)/$(project).service
	systemctl --system daemon-reload
