#ifdef PULSE_SUPPORT

#ifndef __event_mainloop_h__
#define __event_mainloop_h__ "event-mainloop.h"

void event_mainloop_api_init(pa_mainloop_api* api, struct event_base* evb, int pr_io, int pr_time, int pr_defer);
void event_mainloop_api_done(pa_mainloop_api* api);
struct event_base* event_mainloop_api_event_base(pa_mainloop_api* api);

#endif//__event_mainloop_h__

#endif//PULSE_SUPPORT
