* Intro

  This is a simple *TTS* (Text To Speech) service, which takes text through *DBus* interface and gives speech
  through *ALSA* (or *PulseAudio* optionally) to specified device.
  
  It supported plugin interface for *TTS* subsystems.
  Multiple presets can be provided by using config in *JSON* format.
  
  *DBus* interface gives functionality of flow control like appending phrases and removing it from queue.
  Monitoring and notifications also available for client apps.

* Why?
  
  There is two important things about *TTS* subsystems in human interaction. It is a /performance/ and /latency/.
  
  The /performance/ should allow generate sound samples in realtime on target hardware
  (Some *TTS* engines are too slow, so that can't be used on embedded hardware).
  
  The /latency/ should be as low as possible for user, and as high as possible for target system, so that don't
  overheat scheduler.
  
  This service implements a middle layer between applications and TTS software.

* DBus interfaces

  Service name: /org.illumium.speech/.

** Manager

   Object path: */*

   Interface name: /org.illumium.speech.Manager/
   

   Get (Create) renderer instance by connecting TTS preset to an audio output:
   /.GetInstance(preset, device)-> instance_path/

   + /preset/ — required preset of *TTS* backend (typically native language)
     
   + /device/ — required output device (empty string means that default device will be used)

** Speaker
   
   Object path: */preset_device*

   Interface name: /org.illumium.speech.Speaker/
   
   
   Add phrase to a queue: /.Queue(phrase)/
   
   + /phrase/ — string of utf8 encoded text which will be rendered and played


   Remove phrases from beginning of queue: /.Shift(reset, count)/
   
   + if /reset/ is *true*, currently playing phrase (if something playing) will be canceled
   
   + /count/ — a number of phrases that must be dropped from beginning of queue
   (/0/ means that all phrases will be dropped)
   
   
   Get current state: /.State()-> ["active_phrase"],["first_queued_phrase", …, "last_queued_phrase"]/

   
   Deque notification: /.Deque(active, queued)/
   
   + if /active/ is *true*, it has some phrase in playing

   + /queued/ — a number of phrases in queue
