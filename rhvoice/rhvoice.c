#include "../common.h"
#include "rhvoice.h"

synth_api_impl(PROJECT,
               rhvoice_init,
               rhvoice_done,
               rhvoice_op_init,
               rhvoice_op_done);

static int _rhvoice_cb(const short *samples, int num_samples, const RHVoice_event *events, int num_events, RHVoice_message message){
  synth_op_t* op = RHVoice_get_user_data(message);
  size_t bytes = num_samples * sizeof(*samples);
  
  return synth_op_put_block(op, samples, bytes) == bytes ? 1 : 0;
}

__attribute__((constructor)) void _rhvoice_global_init(void){
  if(!RHVoice_initialize(NULL, _rhvoice_cb, NULL, RHVoice_preload_voices)){
    // error
  }
}

__attribute__((destructor)) void _rhvoice_global_done(void){
  RHVoice_terminate();
}

rhvoice_t* rhvoice_init(struct event_base* ev_base, json_object* config){
  rhvoice_t* self = malloc(sizeof(rhvoice_t));
  
  synth_(self, rate) = 16000;
  
#define RHVoice_get_min_volume() 0.0
  
#define _(NAME)                                 \
  self->NAME = RHVoice_get_default_##NAME();    \
  float min_##NAME = RHVoice_get_min_##NAME();  \
  float max_##NAME = RHVoice_get_max_##NAME();
  
  _(rate)_(pitch)_(volume);

#undef _

#define _(NAME) self->NAME = -1;

  _(voice)_(variant);

#undef _
  
  json_object_object_foreach(config, key, val){
    if(json_object_is_type(val, json_type_double)){

#define _(NAME) if(strcmp(#NAME, key) == 0){                  \
        self->NAME = json_object_get_double(val);             \
        if(self->NAME < min_##NAME) self->NAME = min_##NAME;  \
        if(self->NAME > max_##NAME) self->NAME = max_##NAME;  \
      }else
      
      _(rate)_(pitch)_(volume);

#undef _
    }else if(json_object_is_type(val, json_type_string)){
      
#define _(NAME) if(strcmp(#NAME, key) == 0){                            \
        self->NAME = RHVoice_find_##NAME(json_object_get_string(val));  \
      }else
      
      _(voice)_(variant);

#undef _
    }
  }
  
  return self;
}

void rhvoice_done(rhvoice_t* self){
  
}

static void* _rhvoice_thread(void *ud){
  rhvoice_op_t* op = ud;
  rhvoice_t* self = (rhvoice_t*)synth_op_(op, synth);
  
  RHVoice_set_message_rate(op->message, self->rate);
  RHVoice_set_message_pitch(op->message, self->pitch);
  RHVoice_set_message_volume(op->message, self->volume);
  
  RHVoice_set_voice(self->voice);
  RHVoice_set_variant(self->variant);
  
  synth_op_begin(op);
  
  RHVoice_set_user_data(op->message, op);
  RHVoice_speak(op->message);
  
  synth_op_end(op);
  
  return NULL;
}

rhvoice_op_t* rhvoice_op_init(rhvoice_t* synth, const char* text, size_t size){
  rhvoice_op_t* op = malloc(sizeof(rhvoice_op_t));
  
  synth_op_init(op, synth, text, size);
  
  op->message = RHVoice_new_message_utf8((uint8_t*)synth_op_(op, text), strlen(synth_op_(op, text)), RHVoice_message_text);
  
  if(!op->message){
    synth_op_end(op);
    return op;
  }
  
  if(pthread_create(&op->thread, NULL, _rhvoice_thread, op) != 0){
    synth_op_end(op);
  }
  
  return op;
}

void rhvoice_op_done(rhvoice_op_t* op){
  /* block until thread is stop */
  pthread_join(op->thread, NULL);
  
  if(op->message){
    RHVoice_delete_message(op->message);
  }
  
  synth_op_done(op);
  
  free(op);
}
