#ifndef __rhvoice_h__
#define __rhvoice_h__ "rhvoice.h"

#include "../synth.h"

#include <string.h>
#include <pthread.h>
#include <RHVoice.h>

typedef struct rhvoice_ rhvoice_t;
typedef struct rhvoice_op_ rhvoice_op_t;

struct rhvoice_ {
  synth_cookie;
  
  float rate;
  float pitch;
  float volume;
  int voice;
  int variant;
};

struct rhvoice_op_ {
  synth_op_cookie;
  
  RHVoice_message message;
  pthread_t thread;
};

rhvoice_t* rhvoice_init(struct event_base* ev_base, json_object* config);
void rhvoice_done(rhvoice_t* self);

rhvoice_op_t* rhvoice_op_init(rhvoice_t* self, const char* text, size_t size);
void rhvoice_op_done(rhvoice_op_t* op);

#endif//__rhvoice_h__
